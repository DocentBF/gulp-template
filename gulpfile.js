var gulp = require('gulp'),
    sass = require('gulp-sass'),
    plumber = require('gulp-plumber'),
    jshint = require('gulp-jshint'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    cleancss = require('gulp-clean-css'),

    cssnano = require('gulp-cssnano'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    clean = require('gulp-clean'),
    sort = require('gulp-sort'),

    imagemin = require('gulp-image'),
    imageminMozJpeg = require('imagemin-mozjpeg'),
    imageminPngQuant = require('imagemin-pngquant'),
    imageminGifsicle = require('imagemin-gifsicle'),
    imageminSvgo = require('imagemin-svgo'),

    changed = require('gulp-changed'),
    critical = require('critical');

/* CONFIG SECTION */
const buildDir = './build/';
const sourceDir = './source/';
const build = {
    css: buildDir + "css/",
    js: buildDir + "js/",
    images: "./images/",
    fonts: buildDir + "fonts/",
};
const source = {
    styles: sourceDir + "styles/",
    js: sourceDir + "js/",
    lib: sourceDir + "lib/",
    images: "./images/",
    fonts: sourceDir + "fonts/",
};
const siteUrl = "http://www.polimer-s-group.ru/"; // for critical css
/**************************************************/

stylesBuild = function (src, dst, filename) {
    if (!filename)
        var filename = "main.css";

    return gulp.src(src)
        .pipe(sort())
        .pipe(plumber({
            errorHandler: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 10 versions'],
            cascade: false
        }))
        .pipe(concat(filename))
        .pipe(cssnano({
            reduceIdents: false
        }))
        .pipe(cleancss())
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(dst));
};

jsBuild = function (src, dst, filename) {
    if (!filename)
        var filename = "main.min.js";
    return gulp.src(src)
        .pipe(sort())
        .pipe(plumber({
            errorHandler: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(sourcemaps.init())
        //.pipe(jshint())
        //.pipe(jshint.reporter('default'))
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(concat(filename))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(dst));
};

gulp.task('styles', function () {
    var src = [
        source.styles + '**/*.scss',
        source.styles + '**/*.css',
        "!" + source.styles + "**/_*.scss",
        "!" + source.styles + "**/_*.css",
        "!" + source.styles + "_**/*.scss",
        "!" + source.styles + "_**/*.css"
    ];
    var dst = build.css;

    return stylesBuild(src, dst);
});

gulp.task('js', function () {
    var src = [
        source.js + '**/*.js',
        "!" + source.js + "**/_*.js",
        "!" + source.js + "_**/*.js"
    ];
    var dst = build.js;

    return jsBuild(src, dst);
});

gulp.task('imagemin', function () {
    var src = source.images + '**/*.*';

    return gulp.src(src)
        .pipe(changed('./.cache/imagemin'))
        .pipe(imagemin([
            imageminGifsicle({interlaced: true}),
            imageminMozJpeg(),
            imageminPngQuant({quality:'85-100'}),
            imageminSvgo({plugins: [{removeViewBox: true}]})
        ]))
        .pipe(gulp.dest('./.cache/imagemin'))
        .pipe(gulp.dest(build.images));
});

gulp.task('clean', function () {
    return gulp.src([build.css, build.js], {read: false})
        .pipe(clean());
});

gulp.task('libs', function (cb) {
    var cssSrc = [
            source.lib + '**/*.scss',
            source.lib + '**/*.css',
            "!" + source.lib + "**/_*.scss",
            "!" + source.lib + "**/_*.css",
            "!" + source.lib + "_**/*.scss",
            "!" + source.lib + "_**/*.css"
        ],
        jsSrc = [
            source.lib + '**/*.js',
            //"!" + source.lib + "**/*.min.js",
            "!" + source.lib + "**/_*.js",
            "!" + source.lib + "_**/*.js"
        ],
        cssDst = build.css,
        jsDst = build.js;
    stylesBuild(cssSrc, cssDst, "libs.css");
    jsBuild(jsSrc, jsDst, "libs.min.js");

    cb();
});

gulp.task('criticalCSS', function (c) {
    var dst = build.css + "critical.css";
    return critical.generate({
        src: siteUrl,
        dest: dst,
        minify: true,
        timeout: 30000,
        width: 1300,
        height: 700,
        inline: false,
        ignore: [/url\(/, '@font-face'],
    });

});

gulp.task('copy', function (c) {
    gulp.src(source.fonts + "**").pipe(gulp.dest(build.fonts));
    c();
});

gulp.task('watch', function () {
    var styles = [
        source.styles + '**/*.scss',
        source.styles + '**/*.css'
    ];
    var js = [
        //source.js + '*.js',
        source.js + '**/*.js'
    ];
    var images = [
        source.images + '**/*.*'
    ];

    gulp.watch(styles, gulp.parallel('styles'));
    gulp.watch(js, gulp.parallel('js'));
    gulp.watch(images, gulp.parallel('imagemin'));
});

gulp.task('build', gulp.series('clean', gulp.parallel('styles', 'js', 'libs')));
gulp.task('default', gulp.parallel('watch'));